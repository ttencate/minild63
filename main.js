'use strict';
var TILE_SIZE = 24;
var TILES_X = 42;
var TILES_Y = 32;
var WIDTH = TILES_X * TILE_SIZE;
var HEIGHT = TILES_Y * TILE_SIZE;

var ION_INTERVAL = 200; // ms
var ION_SPEED = 200; // px/s
var PLUS_FORCE = 1e3;
var PLUS_SIGMA = 4 * TILE_SIZE;
var GOAL_FORCE = 1e3;
var GOAL_SIGMA = 1 * TILE_SIZE;

var game = new Phaser.Game(WIDTH, HEIGHT, Phaser.AUTO, 'canvas');

var levelNumber = parseInt(window.location.hash.replace('#', ''), 10) || 1;
var NUM_LEVELS = 5;

var GameState = function() {
};

GameState.prototype.init = function() {
  this.player = null;
  this.map = null;
  this.layer = null;
  this.ionGroup = null;
  this.plusGroup = null;
  this.goalsGroup = null;
  this.cursors = null;
  this.dropButton = null;
};

GameState.prototype.preload = function() {
  game.load.image('ion', 'images/ion.png');
  game.load.image('plus', 'images/plus.png');
  game.load.image('goal', 'images/goal.png');
  game.load.image('tiles', 'images/tiles.png');
  game.load.spritesheet('player', 'images/player.png', 48, 48);

  for (var i = 1; i <= NUM_LEVELS; i++) {
    game.load.tilemap('level' + i, 'level' + i + '.json', null, Phaser.Tilemap.TILED_JSON);
  }
  game.load.tilemap('level99', 'level99.json', null, Phaser.Tilemap.TILED_JSON);

  var s = {
    'dead': 'dead2',
    'drop_plus': 'dropPlus',
    'jump': 'jump2',
    'land': 'land',
    'kill_plus': 'pickUpPlus',
    'walk': 'walk',
    'win': 'win',
  };
  for (var k in s) {
    game.load.audio(k, ['sound/' + s[k] + '.mp3', 'sound/' + s[k] + '.ogg']);
  }
};

GameState.prototype.create = function() {
  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.physics.arcade.gravity.y = 1000;

  this.map = game.add.tilemap('level' + levelNumber);
  this.map.addTilesetImage('tiles', 'tiles');

  this.layer = this.map.createLayer(0);
  this.layer.resizeWorld();
  // layer.debug = true; // Enable to see collision bounds

  // Indices (1-based) of tile types that should collide.
  this.map.setCollisionBetween(0, 9);

  this.map.setTileIndexCallback(10, this.refusePlus, this);

  this.ionGroup = game.add.group(undefined, 'ions', false, true, Phaser.Physics.ARCADE);
  this.plusGroup = game.add.group(undefined, 'pluses', false, true, Phaser.Physics.ARCADE);
  this.goalsGroup = game.add.group(undefined, 'goals', false, true, Phaser.Physics.ARCADE);

  for (var y = 0; y < this.map.height; y++) {
    for (var x = 0; x < this.map.width; x++) {
      var tile = this.map.getTile(x, y, 0);
      if (tile && tile.properties.cannon) {
        this.createCannon(x, y, tile.properties.cannon);
      }
    }
  }

  var objects = this.map.objects['objects'];
  objects.forEach(function(object) {
    switch (object.name) {
      case 'player':
        this.player = game.add.sprite(object.x, object.y, 'player');
        this.player.x += this.player.width / 2;
        this.player.anchor.set(0.5, 1);
        this.player.animations.add('stand', [0, 1, 2, 3, 4, 5], 10, true);
        this.player.animations.add('walk', [12, 13, 14, 15, 16, 17], 10, true);
        this.player.animations.play('stand');
        game.physics.enable(this.player, Phaser.Physics.ARCADE);
        this.player.body.setSize(20, 46);
        this.player.body.collideWorldBounds = true;
        this.player.events.onKilled.add(function() {
          this.sounds.dead.play();
          game.state.restart();
        }.bind(this));
        break;
      case 'goal':
        var goal = this.goalsGroup.create(object.x, object.y, 'goal');
        goal.x += goal.width/2;
        goal.y -= goal.height/2;
        goal.body.moves = false;
        goal.anchor.set(0.5, 0.5);
        goal.health = 100;
        goal.events.onKilled.add(function() {
          if (levelNumber == NUM_LEVELS) {
            levelNumber = 99;
          } else {
            levelNumber++;
          }
          this.sounds.win.play();
          window.location.hash = '#' + levelNumber;
          game.state.restart();
        }.bind(this));
        break;
    }
  }.bind(this));

  this.cursors = game.input.keyboard.createCursorKeys();
  this.dropButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  this.dropButton.onDown.add(this.createPlus.bind(this));

  if (levelNumber != 99) {
    this.levelText = game.add.text(12, 12, 'Level ' + levelNumber);
    this.levelText.anchor.set(0, 0);
    this.levelText.font = 'Courier New';
    this.levelText.fontSize = 24;
    this.levelText.fontWeight = 'bold';
    this.levelText.stroke = '#000000';
    this.levelText.strokeThickness = 2;
    this.levelText.fill = '#00ff00';

    this.plusesText = game.add.text(WIDTH - 12, 12, '');
    this.plusesText.anchor.set(1, 0);
    this.plusesText.font = 'Courier New';
    this.plusesText.fontSize = 24;
    this.plusesText.fontWeight = 'bold';
    this.plusesText.stroke = '#000000';
    this.plusesText.strokeThickness = 2;
    this.plusesText.fill = '#00ff00';
  }
  if (levelNumber == 1) {
    var titleText = game.add.text(WIDTH / 2, 192 - 16, 'POLAR BEARER');
    titleText.anchor.set(0.5, 1);
    titleText.font = 'Courier New';
    titleText.fontSize = 48;
    titleText.fontWeight = 'bold';
    titleText.stroke = '#000000';
    titleText.strokeThickness = 4;
    titleText.fill = '#00ff00';

    var introText = game.add.text(WIDTH / 2, HEIGHT - 12, 'Left/right to walk, up to jump, space to drop a repelling charge');
    introText.anchor.set(0.5, 1);
    introText.font = 'Courier New';
    introText.fontSize = 24;
    introText.fontWeight = 'bold';
    introText.stroke = '#000000';
    introText.strokeThickness = 2;
    introText.fill = '#00ff00';
  }

  this.sounds = {
    dead: game.add.audio('dead'),
    dropPlus: game.add.audio('drop_plus'),
    jump: game.add.audio('jump'),
    land: game.add.audio('land'),
    killPlus: game.add.audio('kill_plus'),
    walk: game.add.audio('walk'),
    win: game.add.audio('win'),
  };
  this.sounds.dead.volume = 5;
  this.sounds.jump.volume = 2;
  this.sounds.walk.volume = 0.3;
  this.sounds.walk.loop = true;
  this.sounds.win.volume = 3;
};

GameState.prototype.createCannon = function(x, y, direction) {
  var position = new Phaser.Point((x + 0.5) * TILE_SIZE, (y + 0.5) * TILE_SIZE);
  var velocity = new Phaser.Point();
  switch (direction) {
    case 'up': velocity.set(0, -1); break;
    case 'down': velocity.set(0, 1); break;
    case 'left': velocity.set(-1, 0); break;
    case 'right': velocity.set(1, 0); break;
  }
  velocity.setMagnitude(TILE_SIZE / 2);
  position = Phaser.Point.add(position, velocity);
  velocity.setMagnitude(ION_SPEED);

  function fireIon() {
    var ion = this.ionGroup.getFirstDead(true, position.x, position.y, 'ion');
    ion.anchor.set(0.5, 0.5);
    ion.body.velocity.copyFrom(velocity);
    ion.body.allowGravity = false;
  }

  var timer = game.time.create(false);
  timer.loop(ION_INTERVAL, fireIon, this);
  timer.start();

  timer = game.time.create(false);
  timer.loop(3000, function() {
    this.goalsGroup.forEachAlive(function(goal) { goal.heal(100); });
  }, this);
  timer.start();
};

GameState.prototype.createPlus = function(target, event) {
  var livePluses = 0;
  this.plusGroup.forEachAlive(function() { livePluses++; });
  if (livePluses >= parseInt(this.map.properties.pluses, 10)) return;

  var plus = this.plusGroup.create(this.player.x, this.player.y - 24, 'plus');
  plus.anchor.set(0.5, 0.5);

  plus.body.collideWorldBounds = true;
  plus.body.drag.set(1000, 0);

  plus.inputEnabled = true;
  plus.input.enableDrag(true);

  this.sounds.dropPlus.play();
};

GameState.prototype.refusePlus = function(sprite, tile) {
  if (sprite.key != 'plus') return;
  sprite.kill();
  this.sounds.killPlus.play();
};

GameState.prototype.update = function() {
  if (this.player) {
    game.physics.arcade.collide(this.player, this.layer);
    var onFloor = this.player.body.onFloor();
    if (onFloor && !this.player.wasOnFloor) {
      this.sounds.land.play();
    }
    this.player.wasOnFloor = onFloor;
    if (this.cursors.left.isDown) {
      this.player.animations.play('walk');
      this.player.body.velocity.x = -200;
      this.player.scale.x = -1;
    } else if (this.cursors.right.isDown) {
      this.player.animations.play('walk');
      this.player.body.velocity.x = 200;
      this.player.scale.x = 1;
    } else {
      this.player.body.velocity.x = 0;
      this.player.animations.play('stand');
    }
    var walking = onFloor && this.player.body.velocity.x;
    if (walking && !this.sounds.walk.isPlaying) {
      this.sounds.walk.loopFull();
    } else if (!walking && this.sounds.walk.isPlaying) {
      this.sounds.walk.stop();
    }
    if (this.cursors.up.isDown) {
      var P = 500;
      var N = 5;
      if (this.player.body.onFloor()) {
        this.player.body.velocity.y = -P/N;
        this.player.jumpPower = N - 1;
        this.sounds.jump.play();
      } else if (this.player.jumpPower > 0) {
        this.player.jumpPower--;
        this.player.body.velocity.y -= P/N;
      }
    } else {
      this.player.jumpPower = 0;
    }
  }

  game.physics.arcade.collide(this.plusGroup, this.layer);

  game.physics.arcade.collide(this.player, this.plusGroup, function(player, plus) {
    if (player.body.touching.down) {
      plus.scale.y *= 0.9;
      plus.scale.x /= 0.9;
      if (plus.scale.y < 0.7) {
        plus.kill();
        this.sounds.killPlus.play();
      }
    }
  }.bind(this));

  this.ionGroup.forEach(function(ion) {
    var ax = 0;
    var ay = 0;
    this.plusGroup.forEach(function(plus) {
      if (!plus.alive) return;
      var dx = ion.position.x - plus.position.x;
      var dy = ion.position.y - plus.position.y;
      var r = Math.sqrt(dx*dx + dy*dy);
      var sigma = r / PLUS_SIGMA;
      var a = PLUS_FORCE * Math.pow(Math.E, -sigma*sigma);
      ax += a * dx / r;
      ay += a * dy / r;
    }.bind(this));
    this.goalsGroup.forEach(function(goal) {
      var dx = ion.position.x - goal.position.x;
      var dy = ion.position.y - goal.position.y;
      var r = Math.sqrt(dx*dx + dy*dy);
      if (r < 72) {
        ax += -1e4 * dx / r;
        ay += -1e4 * dy / r;
      }
    }.bind(this));
    ion.body.acceleration.set(ax, ay);
  }.bind(this));

  game.physics.arcade.collide(this.ionGroup, this.goalsGroup, function(ion, goal) {
    ion.kill();
    goal.damage(10);
  });

  game.physics.arcade.collide(this.ionGroup, this.layer, function(ion) {
    ion.kill();
  });

  game.physics.arcade.collide(this.player, this.ionGroup, function(player) {
    player.kill();
  });

  if (this.plusesText) {
    var remainingPluses = parseInt(this.map.properties.pluses, 10);
    this.plusGroup.forEachAlive(function() { remainingPluses--; });
    this.plusesText.text = remainingPluses + ' charge' + (remainingPluses == 1 ? '' : 's') + ' left';
  }
};

GameState.prototype.shutdown = function() {
  this.sounds.walk.stop();
};

game.state.add('game', new GameState());
game.state.start('game');
